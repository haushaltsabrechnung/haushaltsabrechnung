Diese Anleitung erläutert die notwendigen Schritte, um eine funktionsfähige
Instanz der Anwendung einzurichten.

1.) eXist 1.4.1 installieren

Falls noch nicht geschehen sollte die Version 1.4.1 von eXist unter 
http://exist-db.org/download.html heruntegeladen und nach der entsprechenden
Installationsanleitung installiert werden. Es wird im Folgenden davon
ausgegangen, dass eXist nach ${EXIST_HOME} installiert wurde.

2.) eXist auf XSLT 2.0 umstellen

Die Anwendung läuft mit XSLT 2.0 Stylesheets. Um den standard XSLT Prozessor (Xalan) durch einen Prozessor 
zu ersetzen der XSLT 2.0 unterstützt sind die folgenden Schritte notwendig:

	a) Notwendige JARs:
	 Saxon-B herunterladen (z.B. von http://sourceforge.net/projects/saxon/files/Saxon-B/) und die Dateien
	"saxon9.jar", "saxon9-dom.jar" und "saxon9-xpath.jar" in das Verzeichnis ${EXIST_HOME}/lib/user kopieren.
	
	b) Konfiguration von eXist: In der Datei ${EXIST_HOME}/conf.xml die Zeile 
	<transformer class="org.apache.xalan.processor.TransformerFactoryImpl" />
	
	umändern in
	
	<transformer class="net.sf.saxon.TransformerFactoryImpl" />
	
3.) Sourcecode auschecken

Falls noch nicht geschehen sollte der Sourcecode von
https://svn.iai.uni-bonn.de/repos/wob/pg_s11 ausgecheckt werden. Es wird im
Folgenden davon ausgegangen, dass die Working Copy in ${WORKSPACE} liegt.

4.) Webapp kopieren

Als nächstes sollte das Verzeichnis ${WORKSPACE}/ab nach ${EXIST_HOME}/webapp
kopiert oder verlinkt werden. Unter Unixoiden Betriebssystemen geschieht dies
mittels
ln -s ${WORKSPACE}/ab ${EXIST_HOME}/webapp

HINWEIS: Damit diese Methode funktioniert muss zusätzlich in der Datei
${EXIST_HOME}/tools/jetty/etc/jetty.xml der Wert von
<Setclass="org.mortbay.util.FileResource" name="checkAliases" type="boolean">
von 'true' auf 'false' gesetzt werden.

5.) eXist starten

Als nächstes muss der eXist-Server gestartet werden.

6.) Datenbank vorbereiten

a) Collection einrichten

Standardmäßig ist das Admin-Webfrontend von eXist unter
http://localhost:8080/exist/admin/admin.xql zu erreichen. Nach einer Anmeldung
mit dem Login, der während der eXist-Installation eingerichtet hat, ist 
unter "Browse Collections" eine neue Collection namens "ab" anzulegen.

b) Initialdaten hochladen

In diese Collection müssen die Dateien demands.xml, groups.xml, messages.xml,
transactions.xml, standing-orders.xml und users.xml aus dem Verzeichnis
${WORKSPACE}/projekt-beschreibung/beispieldaten hochgeladen werden.

c) Benutzer einrichten

Im Menüpunkt "User Management" ist ein neuer Benutzer anzulegen. Der
Benutzername muss bereits als ID eines Users in der users.xml vorhanden sein.
Das Passwort kann beliebig gewählt werden, als Gruppe ist 'ab' zu setzen. Eine
Home-Collection wird nicht benötigt.

7.) Testen

Nun kann die Anwendung unter http://localhost:8080/exist/ab/login.xql
aufgerufen werden. Viel Spaß beim Testen :)
